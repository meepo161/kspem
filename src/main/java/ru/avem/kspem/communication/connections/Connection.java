package ru.avem.kspem.communication.connections;

public interface Connection {
    boolean initConnection();

    void closeConnection();

    boolean isInitiatedConnection();

    int write(byte[] outputArray);

    int read(byte[] inputArray);
}
