package ru.avem.kspem.communication;

import ru.avem.kspem.Constants;
import ru.avem.kspem.communication.connections.Connection;
import ru.avem.kspem.communication.connections.SerialConnection;
import ru.avem.kspem.communication.devices.DeviceController;
import ru.avem.kspem.communication.devices.deltaC2000.DeltaCP2000Controller;
import ru.avem.kspem.communication.devices.m40.M40Controller;
import ru.avem.kspem.communication.devices.pm130.PM130Controller;
import ru.avem.kspem.communication.devices.pr200.OwenPRController;
import ru.avem.kspem.communication.devices.trm.TRMController;
import ru.avem.kspem.communication.modbus.ModbusController;
import ru.avem.kspem.communication.modbus.RTUController;
import ru.avem.kspem.utils.Logger;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import static ru.avem.kspem.communication.devices.DeviceController.*;
import static ru.avem.kspem.communication.devices.deltaC2000.DeltaCP2000Controller.*;
import static ru.avem.kspem.communication.devices.pr200.OwenPRController.*;
import static ru.avem.kspem.utils.Utils.sleep;


public class CommunicationModel extends Observable implements Observer {

    private static CommunicationModel instance = new CommunicationModel();

    private Connection RS485Connection;

    private OwenPRController owenPRController;
    private PM130Controller pm130Controller;
    private DeltaCP2000Controller deltaCP2000ObjectController;
    private DeltaCP2000Controller deltaCP2000GeneratorController;
    private TRMController trmController;
    private M40Controller m40Controller;

    private int kms1;

    private boolean lastOne;
    private boolean isFinished;

    private List<DeviceController> devicesControllers = new ArrayList<>();

    private CommunicationModel() {
        RS485Connection = new SerialConnection(
                Constants.Communication.RS485_DEVICE_NAME,
                Constants.Communication.BAUDRATE,
                Constants.Communication.DATABITS,
                Constants.Communication.STOPBITS,
                Constants.Communication.PARITY,
                Constants.Communication.WRITE_TIMEOUT,
                Constants.Communication.READ_TIMEOUT);

        ModbusController modbusController = new RTUController(RS485Connection);


        pm130Controller = new PM130Controller(1, this, modbusController, PM130_ID);
        devicesControllers.add(pm130Controller);

        trmController = new TRMController(2, this, modbusController, TRM_ID);
        devicesControllers.add(trmController);

        m40Controller = new M40Controller(3, this, modbusController, M40_ID);
        devicesControllers.add(m40Controller);

        owenPRController = new OwenPRController(4, this, modbusController, PR200_ID);
        devicesControllers.add(owenPRController);

        deltaCP2000ObjectController = new DeltaCP2000Controller(6, this, modbusController, DELTACP2000Object_ID);
        devicesControllers.add(deltaCP2000ObjectController);

        deltaCP2000GeneratorController = new DeltaCP2000Controller(5, this, modbusController, DELTACP2000Generator_ID);
        devicesControllers.add(deltaCP2000GeneratorController);


        new Thread(() -> {
            while (!isFinished) {
                for (DeviceController deviceController : devicesControllers) {
                    if (deviceController.needToRead()) {
                        if (deviceController instanceof PM130Controller) {
                            for (int i = 1; i <= 4; i++) {
                                deviceController.read(i);
                            }
                        } else {
                            deviceController.read();
                        }
                        if (deviceController instanceof OwenPRController) {
                            resetDog();
                        }
                    }
                }
                sleep(1);
            }
        }).start();
        connectMainBus();
    }

    public static CommunicationModel getInstance() {
        return instance;
    }

    public void setFinished(boolean finished) {
        isFinished = finished;
    }

    private void notice(int deviceID, int param, Object value) {
        setChanged();
        notifyObservers(new Object[]{deviceID, param, value});
    }

    @Override
    public void update(Observable o, Object values) {
        int modelId = (int) (((Object[]) values)[0]);
        int param = (int) (((Object[]) values)[1]);
        Object value = (((Object[]) values)[2]);
        notice(modelId, param, value);
    }

    public void initAllDevices() {
        initOwenPrController();
        readAndResetDeltaObject();
        readAndResetDeltaGenerator();
        readAndResetPM130();
        readAndResetTrm();
        readAndResetM40();

    }

    public void resetAllDevices() {
        owenPRController.setNeedToRead(true);
        owenPRController.resetReadAttempts();
        owenPRController.resetWriteAttempts();
        deltaCP2000ObjectController.setNeedToRead(true);
        deltaCP2000ObjectController.resetReadAttempts();
        deltaCP2000ObjectController.resetWriteAttempts();
        deltaCP2000GeneratorController.setNeedToRead(true);
        deltaCP2000GeneratorController.resetReadAttempts();
        deltaCP2000GeneratorController.resetWriteAttempts();
        pm130Controller.setNeedToRead(true);
        pm130Controller.resetReadAttempts();
        pm130Controller.resetWriteAttempts();
        trmController.setNeedToRead(true);
        trmController.resetReadAttempts();
        trmController.resetWriteAttempts();
        m40Controller.setNeedToRead(true);
        m40Controller.resetReadAttempts();
        m40Controller.resetWriteAttempts();
    }

    public void readAndResetTrm() {
        trmController.setNeedToRead(false);
        trmController.resetReadAttempts();
        trmController.resetWriteAttempts();
        trmController.setNeedToRead(true);
        trmController.resetReadAttempts();
        trmController.resetWriteAttempts();
    }


    public void readAndResetDeltaObject() {
        deltaCP2000ObjectController.setNeedToRead(false);
        deltaCP2000ObjectController.resetReadAttempts();
        deltaCP2000ObjectController.resetWriteAttempts();
        deltaCP2000ObjectController.setNeedToRead(true);
        deltaCP2000ObjectController.resetReadAttempts();
        deltaCP2000ObjectController.resetWriteAttempts();
    }

    public void readAndResetDeltaGenerator() {
        deltaCP2000GeneratorController.setNeedToRead(false);
        deltaCP2000GeneratorController.resetReadAttempts();
        deltaCP2000GeneratorController.resetWriteAttempts();
        deltaCP2000GeneratorController.setNeedToRead(true);
        deltaCP2000GeneratorController.resetReadAttempts();
        deltaCP2000GeneratorController.resetWriteAttempts();
    }

    public void readAndResetPM130() {
        pm130Controller.setNeedToRead(false);
        pm130Controller.resetReadAttempts();
        pm130Controller.resetWriteAttempts();
        pm130Controller.setNeedToRead(true);
        pm130Controller.resetReadAttempts();
        pm130Controller.resetWriteAttempts();
    }

    public void readAndResetM40() {
        m40Controller.setNeedToRead(false);
        m40Controller.resetReadAttempts();
        m40Controller.resetWriteAttempts();
        m40Controller.setNeedToRead(true);
        m40Controller.resetReadAttempts();
        m40Controller.resetWriteAttempts();
    }

    private void connectMainBus() {
        Logger.withTag("DEBUG_TAG").log("connectMainBus");
        if (!RS485Connection.isInitiatedConnection()) {
            Logger.withTag("DEBUG_TAG").log("!isInitiatedMainBus");
            RS485Connection.initConnection();
        }
    }

    public void reInitOwenPR() {
        owenPRController.setNeedToRead(false);
        sleep(100);
        owenPRController.setNeedToRead(true);
        owenPRController.resetReadAttempts();
        owenPRController.resetWriteAttempts();
        owenPRController.write(RES_REGISTER, 1, 1);
    }

    public void finalizeDevicesWithoutPR() {
        pm130Controller.setNeedToRead(false);
        trmController.setNeedToRead(false);
        deltaCP2000ObjectController.setNeedToRead(false);
        deltaCP2000GeneratorController.setNeedToRead(false);
        m40Controller.setNeedToRead(false);
    }

    private void disconnectMainBus() {
        RS485Connection.closeConnection();
    }

    public void initOwenPRforCurrentProtection() {
        owenPRController.setNeedToRead(true);
    }

    public void finalizeAllDevices() {
        owenPRController.write(RES_REGISTER, 1, 0);
        offAllKms();
        for (DeviceController deviceController : devicesControllers) {
            deviceController.setNeedToRead(false);
        }
    }

    private void resetDog() {
        Logger.withTag("StatusActivity").log("Dog on");
        owenPRController.write(RESET_DOG, 1, 1);
    }


    public void offAllKms() {
        kms1 = 0;
        writeToKms1Register(kms1);
    }

    private void writeToKms1Register(int value) {
        owenPRController.write(KMS1_REGISTER, 1, value);
    }

    private void writeToKms2Register(int value) {
        owenPRController.write(KMS2_REGISTER, 1, value);
    }

    private void writeToKms3Register(int value) {
        owenPRController.write(KMS3_REGISTER, 1, value);
    }


    public void onRegisterInTheKms(int numberOfRegister, int kms) {
        int mask = (int) Math.pow(2, --numberOfRegister);
        try {
            int kmsField = CommunicationModel.class.getDeclaredField("kms" + kms).getInt(this);
            kmsField |= mask;
            CommunicationModel.class.getDeclaredMethod(String.format("%s%d%s", "writeToKms", kms, "Register"), int.class).invoke(this, kmsField);
            CommunicationModel.class.getDeclaredField("kms" + kms).set(this, kmsField);
        } catch (NoSuchFieldException | IllegalAccessException | NoSuchMethodException | InvocationTargetException ignored) {
        }
        Logger.withTag("DEBUG_TAG").log("numberOfRegister=" + numberOfRegister + " kms=" + kms);
        Logger.withTag("DEBUG_TAG").log("1=" + kms1);
    }

    public void offRegisterInTheKms(int numberOfRegister, int kms) {
        int mask = ~(int) Math.pow(2, --numberOfRegister);
        try {
            int kmsField = CommunicationModel.class.getDeclaredField("kms" + kms).getInt(this);
            kmsField &= mask;
            CommunicationModel.class.getDeclaredMethod(String.format("%s%d%s", "writeToKms", kms, "Register"), int.class).invoke(this, kmsField);
            CommunicationModel.class.getDeclaredField("kms" + kms).set(this, kmsField);
        } catch (NoSuchFieldException | IllegalAccessException | InvocationTargetException | NoSuchMethodException ignored) {
        }
        Logger.withTag("DEBUG_TAG").log("numberOfRegister=" + numberOfRegister + " kms=" + kms);
        Logger.withTag("DEBUG_TAG").log("1=" + kms1);
    }

    public void initOwenPrController() {
        offAllKms();
        owenPRController.setNeedToRead(true);
        owenPRController.resetReadAttempts();
        owenPRController.resetWriteAttempts();
        owenPRController.write(RES_REGISTER, 1, 1);
    }

    public void startObject() {
        deltaCP2000ObjectController.write(CONTROL_REGISTER, 1, 0b10);
    }

    public void startReversObject() {
        deltaCP2000ObjectController.write(CONTROL_REGISTER, 1, 0b10_00_10);
    }

    public void stopObject() {
        deltaCP2000ObjectController.write(CONTROL_REGISTER, 1, 0b1);
    }

    public void setObjectParams(int fOut, int voltageP1, int fP1, int voltageP2, int fP2) {
        deltaCP2000ObjectController.write(MAX_VOLTAGE_REGISTER, 1, 335 * 10);
        deltaCP2000ObjectController.write(MAX_FREQUENCY_REGISTER, 1, 501 * 100);
        deltaCP2000ObjectController.write(NOM_FREQUENCY_REGISTER, 1, 501 * 100);
        deltaCP2000ObjectController.write(CURRENT_FREQUENCY_OUTPUT_REGISTER, 1, fOut);
        deltaCP2000ObjectController.write(P2_VOLTAGE_REGISTER, 1, voltageP2);
        deltaCP2000ObjectController.write(P2_FREQUENCY_REGISTER, 1, fP2);
        deltaCP2000ObjectController.write(P1_VOLTAGE_REGISTER, 1, voltageP1);
        deltaCP2000ObjectController.write(P1_FREQUENCY_REGISTER, 1, fP1);
    }

    public void setObjectFCur(int fCur) {
        deltaCP2000ObjectController.write(FREQUENCY_RS485, 1, fCur);
    }

    public void setObjectUMax(int voltageMax) {
        deltaCP2000ObjectController.write(FREQUENCY_RS485, 1, voltageMax);
    }

    public void startGenerator() {
        deltaCP2000GeneratorController.write(CONTROL_REGISTER, 1, 0b10);
    } //0x1E

    public void changeRotationGenerator() {
        deltaCP2000GeneratorController.write(CONTROL_REGISTER, 1, 0b0011_0000); //0b10_00_10
    }

    public void stopGenerator() {
        deltaCP2000GeneratorController.write(CONTROL_REGISTER, 1, 0b1);
    } //0b1

    public void setGeneratorParams(int fOut, int voltageP1, int fP1, int voltageP2, int fP2) {
        deltaCP2000GeneratorController.write(MAX_VOLTAGE_REGISTER, 1, 340 * 10);
        deltaCP2000GeneratorController.write(MAX_FREQUENCY_REGISTER, 1, 501 * 100);
        deltaCP2000GeneratorController.write(NOM_FREQUENCY_REGISTER, 1, 501 * 100);
        deltaCP2000GeneratorController.write(CURRENT_FREQUENCY_OUTPUT_REGISTER, 1, fOut);
        deltaCP2000GeneratorController.write(P2_VOLTAGE_REGISTER, 1, voltageP2);
        deltaCP2000GeneratorController.write(P2_FREQUENCY_REGISTER, 1, fP2);
        deltaCP2000GeneratorController.write(P1_VOLTAGE_REGISTER, 1, voltageP1);
        deltaCP2000GeneratorController.write(P1_FREQUENCY_REGISTER, 1, fP1);
    }

    public void setGeneratorFCur(int fCur) {
        deltaCP2000GeneratorController.write(FREQUENCY_RS485, 1, fCur);
    }

    public void setGeneratorUMax(int voltageMax) {
        deltaCP2000GeneratorController.write(P1_VOLTAGE_REGISTER, 1, voltageMax);
    }

    public void initExperiment1Devices() {
        initOwenPrController();
        readAndResetPM130();
        readAndResetDeltaObject();
        readAndResetDeltaGenerator();
        readAndResetM40();
    }

    public void resetK1_2() {
        onKM8ResetK1_2();
        offKM8ResetK1_2();
    }

    public void onKM1Sound() {
        onRegisterInTheKms(1, 1);
    }

    public void onKM2Light() {
        onRegisterInTheKms(2, 1);
    }

    public void onKM3_KM3_1() {
        onRegisterInTheKms(3, 1);
        sleep(500);
    }

    public void onKM4_KM2_1() {
        onRegisterInTheKms(4, 1);
        sleep(500);
    }

    public void onKM5_KM1_1() {
        onRegisterInTheKms(5, 1);
        sleep(500);
    }

    public void onKM6ButtonPost() {
        onRegisterInTheKms(6, 1);
        sleep(500);
    }

    public void onKM7ErrorLED() {
        onRegisterInTheKms(7, 1);
        sleep(500);
    }

    public void onKM8ResetK1_2() {
        onRegisterInTheKms(1, 1);
        sleep(500);
    }

    public void offKM1Sound() {
        sleep(2500);
        onRegisterInTheKms(1, 1);
        sleep(500);
    }

    public void offKM2Light() {
        onRegisterInTheKms(2, 1);
        sleep(500);
    }

    public void offKM3_KM3_1() {
        onRegisterInTheKms(3, 1);
        sleep(500);
    }

    public void offKM4_KM2_1() {
        onRegisterInTheKms(4, 1);
        sleep(500);
    }

    public void offKM5_KM1_1() {
        onRegisterInTheKms(5, 1);
        sleep(500);
    }

    public void offKM6ButtonPost() {
        onRegisterInTheKms(6, 1);
        sleep(500);
    }

    public void offKM7ErrorLED() {
        onRegisterInTheKms(7, 1);
        sleep(500);
    }

    public void offKM8ResetK1_2() {
        onRegisterInTheKms(1, 1);
        sleep(500);
    }

    public float getTRMTemperature() {
        return trmController.getTemperature();
    }
}

