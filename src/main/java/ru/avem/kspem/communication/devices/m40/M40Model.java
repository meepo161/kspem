package ru.avem.kspem.communication.devices.m40;

import java.util.Observable;
import java.util.Observer;

public class M40Model extends Observable {
    public static final int RESPONDING_PARAM = 0;
    public static final int TORQUE_PARAM = 1;
    public static final int ROTATION_FREQUENCY_PARAM = 2;

    private int deviceID;
    private boolean readResponding;
    private boolean writeResponding;

    M40Model(Observer observer, int deviceID) {
        addObserver(observer);
        this.deviceID = deviceID;
    }


    void setReadResponding(boolean readResponding) {
        this.readResponding = readResponding;
        setResponding();
    }

    void setWriteResponding(boolean writeResponding) {
        this.writeResponding = writeResponding;
        setResponding();
    }

    private void setResponding() {
        notice(RESPONDING_PARAM, readResponding && writeResponding);
    }



    void setTorque(float torque) {
        notice(TORQUE_PARAM, Math.abs(torque));
    }

    void setRotationFrequency(float rotationFrequency) {
        notice(ROTATION_FREQUENCY_PARAM, rotationFrequency);
    }

    private void notice(int param, Object value) {
        setChanged();
        notifyObservers(new Object[]{deviceID, param, value});
    }
}