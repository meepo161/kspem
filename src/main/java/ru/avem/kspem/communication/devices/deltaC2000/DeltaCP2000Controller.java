package ru.avem.kspem.communication.devices.deltaC2000;

import ru.avem.kspem.communication.devices.DeviceController;
import ru.avem.kspem.communication.modbus.ModbusController;

import java.nio.ByteBuffer;
import java.util.Observer;

public class DeltaCP2000Controller implements DeviceController {
    private static final short ERRORS_REGISTER = 0x2100;
    private static final short STATUS_REGISTER = 0x2101;
    private static final short CURRENT_FREQUENCY_INPUT_REGISTER = 0x2103;
    public static final short CONTROL_REGISTER = 0x2000;
    public static final short CURRENT_FREQUENCY_OUTPUT_REGISTER = 0x2001;
    public static final short MAX_FREQUENCY_REGISTER = 0x0100;
    public static final short NOM_FREQUENCY_REGISTER = 0x0101;
    public static final short MAX_VOLTAGE_REGISTER = 0x0102;
    public static final short P1_FREQUENCY_REGISTER = 0x0103;
    public static final short P1_VOLTAGE_REGISTER = 0x0104; //mid-point-volt
    public static final short P2_FREQUENCY_REGISTER = 0x0105;
    public static final short P2_VOLTAGE_REGISTER = 0x0106; //min-output-volt
    public static final short FREQUENCY_RS485 = 0x020C;

    private static final int NUM_OF_WORDS_IN_REGISTER = 1;
    private static final short NUM_OF_REGISTERS = 4 * NUM_OF_WORDS_IN_REGISTER;

    private DeltaCP2000Model model;
    private byte address;
    private ModbusController modbusController;
    private byte readAttempt = NUMBER_OF_READ_ATTEMPTS;
    private byte writeAttempt = NUMBER_OF_WRITE_ATTEMPTS;
    private boolean needToReed;

    public DeltaCP2000Controller(int address, Observer observer, ModbusController controller, int deviceID) {
        this.address = (byte) address;
        model = new DeltaCP2000Model(observer, deviceID);
        modbusController = controller;
    }

    @Override
    public void read(Object... args) {
        ByteBuffer inputBuffer = ByteBuffer.allocate(INPUT_BUFFER_SIZE);
        if (thereAreReadAttempts()) {
            readAttempt--;
            ModbusController.RequestStatus status = modbusController.readMultipleHoldingRegisters(
                    address, ERRORS_REGISTER, NUM_OF_REGISTERS, inputBuffer);
            if (status.equals(ModbusController.RequestStatus.FRAME_RECEIVED)) {
                model.setReadResponding(true);
                model.setErrors(inputBuffer.getShort());
                inputBuffer.getShort();
                inputBuffer.getShort();
                model.setCurrentFrequency(inputBuffer.getShort());
                resetReadAttempts();
            } else {
                read(args);
            }
        }
        else {
            model.setReadResponding(false);
        }
    }

    @Override
    public void write(Object... args) {
        short register = (short) args[0];
        int numOfRegisters = (int) args[1];
        ByteBuffer inputBuffer = ByteBuffer.allocate(INPUT_BUFFER_SIZE);
        ByteBuffer dataBuffer = ByteBuffer.allocate(4 * numOfRegisters);
        for (int i = 2; i < numOfRegisters + 2; i++) {
            dataBuffer.putShort((short) ((int) args[i]));
        }
        dataBuffer.flip();

        if (thereAreWriteAttempts()) {
            writeAttempt--;
            ModbusController.RequestStatus status = modbusController.writeMultipleHoldingRegisters(
                    address, register, (short) numOfRegisters, dataBuffer, inputBuffer);
            if (status.equals(ModbusController.RequestStatus.PORT_NOT_INITIALIZED)) {
                return;
            }
            if (status.equals(ModbusController.RequestStatus.FRAME_RECEIVED)) {
                model.setWriteResponding(true);
                resetWriteAttempts();
            } else {
                write(args);
            }
        }
        else {
            model.setWriteResponding(false);
        }
    }

    @Override
    public void resetReadAttempts() {
        readAttempt = NUMBER_OF_READ_ATTEMPTS;
    }

    @Override
    public boolean thereAreReadAttempts() {
        return readAttempt > 0;
    }

    @Override
    public void resetWriteAttempts() {
        writeAttempt = NUMBER_OF_WRITE_ATTEMPTS;
    }

    @Override
    public boolean thereAreWriteAttempts() {
        return writeAttempt > 0;
    }

    @Override
    public boolean needToRead() {
        return needToReed;
    }

    @Override
    public void setNeedToRead(boolean needToRead) {
        if (needToRead) {
            model.setWriteResponding(true);
        }
        needToReed = needToRead;
    }
}