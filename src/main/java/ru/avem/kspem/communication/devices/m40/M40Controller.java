package ru.avem.kspem.communication.devices.m40;

import ru.avem.kspem.communication.devices.DeviceController;
import ru.avem.kspem.communication.modbus.ModbusController;

import java.nio.ByteBuffer;
import java.util.Observer;

public class M40Controller implements DeviceController {
    private static final short TORQUE_REGISTER = 0;
    public static final short AVERAGING_REGISTER = 1;

    private static final int CONVERT_BUFFER_SIZE = 4;
    private static final int NUM_OF_WORDS_IN_REGISTER = 2;
    private static final short NUM_OF_REGISTERS = 2 * NUM_OF_WORDS_IN_REGISTER;

    private M40Model model;
    private byte address;
    private ModbusController modbusController;
    private byte readAttempt = NUMBER_OF_READ_ATTEMPTS;
    private byte writeAttempt = NUMBER_OF_WRITE_ATTEMPTS;
    private boolean needToReed;


    public M40Controller(int address, Observer observer, ModbusController controller, int deviceID) {
        this.address = (byte) address;
        model = new M40Model(observer, deviceID);
        modbusController = controller;
    }

    @Override
    public void read(Object... args) {
        ByteBuffer inputBuffer = ByteBuffer.allocate(INPUT_BUFFER_SIZE);
        if (thereAreReadAttempts()) {
            readAttempt--;
            ModbusController.RequestStatus status = modbusController.readInputRegisters(
                    address, TORQUE_REGISTER, NUM_OF_REGISTERS, inputBuffer);
            if (status.equals(ModbusController.RequestStatus.FRAME_RECEIVED)) {
                model.setReadResponding(true);
                resetReadAttempts();
                model.setTorque(convertToMidLittleEndian(inputBuffer.getInt()) * 1.025f);
                model.setRotationFrequency(convertToMidLittleEndian(inputBuffer.getInt()));
            } else {
                read(args);
            }
        }
        else {
            model.setReadResponding(false);
            model.setTorque(0);
            model.setRotationFrequency(0);
        }
    }

    @Override
    public void write(Object... args) {
        ByteBuffer inputBuffer = ByteBuffer.allocate(INPUT_BUFFER_SIZE);
        if (thereAreWriteAttempts()) {
            writeAttempt--;
            ModbusController.RequestStatus status = modbusController.writeSingleHoldingRegister(address,
                    (short) args[0], shortToByteArray((short) args[1]), inputBuffer);
            if (status.equals(ModbusController.RequestStatus.FRAME_RECEIVED)) {
                model.setWriteResponding(true);
                resetWriteAttempts();
            } else {
                write(args);
            }
        }
        else {
            model.setWriteResponding(false);
        }
    }

    private byte[] shortToByteArray(short s) {
        ByteBuffer convertBuffer = ByteBuffer.allocate(2);
        convertBuffer.clear();
        return convertBuffer.putShort(s).array();
    }

    @Override
    public void resetReadAttempts() {
        readAttempt = NUMBER_OF_READ_ATTEMPTS;
    }

    @Override
    public boolean thereAreReadAttempts() {
        return readAttempt > 0;
    }

    @Override
    public void resetWriteAttempts() {
        writeAttempt = NUMBER_OF_WRITE_ATTEMPTS;
    }

    @Override
    public boolean thereAreWriteAttempts() {
        return writeAttempt > 0;
    }

    @Override
    public boolean needToRead() {
        return needToReed;
    }

    @Override
    public void setNeedToRead(boolean needToRead) {
        if (needToRead) {
            model.setWriteResponding(true);
        }
        needToReed = needToRead;
    }

    private float convertToMidLittleEndian(int i) {
        ByteBuffer convertBuffer = ByteBuffer.allocate(CONVERT_BUFFER_SIZE);
        convertBuffer.clear();
        convertBuffer.putInt(i);
        convertBuffer.flip();
        short rightSide = convertBuffer.getShort();
        short leftSide = convertBuffer.getShort();
        convertBuffer.clear();
        convertBuffer.putShort(leftSide);
        convertBuffer.putShort(rightSide);
        convertBuffer.flip();
        return convertBuffer.getFloat();
    }

}