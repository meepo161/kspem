package ru.avem.kspem.communication.devices.pr200;

import java.util.Observable;
import java.util.Observer;

public class OwenPRModel extends Observable {
    public static final int RESPONDING_PARAM = 0;
    public static final int DI1_DOORS = 1;
    public static final int DI2_OI = 2;
    public static final int DI3_PROTECTION = 3;
    public static final int DI5_K1_2 = 4;
    public static final int DI6_KM1_2 = 5;
    public static final int DI7_KM2_2 = 6;
    public static final int DI8_KM3_2 = 7;

    public static final int DI1_DOORS_FIXED = 8;
    public static final int DI2_OI_FIXED = 9;
    public static final int DI3_PROTECTION_FIXED = 10;
    public static final int DI5_K1_2_FIXED = 11;
    public static final int DI6_KM1_2_FIXED = 12;
    public static final int DI7_KM2_2_FIXED = 13;
    public static final int DI8_KM3_2_FIXED = 14;

    private int deviceID;
    private boolean readResponding;
    private boolean writeResponding;

    OwenPRModel(Observer observer, int deviceID) {
        addObserver(observer);
        this.deviceID = deviceID;
    }

    void setReadResponding(boolean readResponding) {
        this.readResponding = readResponding;
        setResponding();
    }

    void setWriteResponding(boolean writeResponding) {
        this.writeResponding = writeResponding;
        setResponding();
    }

    private void setResponding() {
        notice(RESPONDING_PARAM, readResponding && writeResponding);
    }

    void setInstantInputStatus(short instantInputStatusInputStatus) {
        noticeInputStatus(instantInputStatusInputStatus,
                DI1_DOORS,
                DI2_OI,
                DI3_PROTECTION,
                DI5_K1_2,
                DI6_KM1_2,
                DI7_KM2_2,
                DI8_KM3_2);
    }

    void setFixedInputStatus(short fixedInputStatus) {
        noticeInputStatus(fixedInputStatus,
                DI1_DOORS_FIXED,
                DI2_OI_FIXED,
                DI3_PROTECTION_FIXED,
                DI5_K1_2_FIXED,
                DI6_KM1_2_FIXED,
                DI7_KM2_2_FIXED,
                DI8_KM3_2_FIXED);
    }

    private void noticeInputStatus(short instantInputStatusInputStatus, int di1Doors, int di2Oi, int di3Protection, int di5K12, int di6Km12, int di7Km22, int di8Km32) {
        notice(di1Doors, (instantInputStatusInputStatus & 0b1) > 0);
        notice(di2Oi, (instantInputStatusInputStatus & 0b10) > 0);
        notice(di3Protection, (instantInputStatusInputStatus & 0b100) > 0);
        notice(di5K12, (instantInputStatusInputStatus & 0b10000) > 0);
        notice(di6Km12, (instantInputStatusInputStatus & 0b100000) > 0);
        notice(di7Km22, (instantInputStatusInputStatus & 0b1000000) > 0);
        notice(di8Km32, (instantInputStatusInputStatus & 0b1000000) > 0);
    }


    private void notice(int param, Object value) {
        setChanged();
        notifyObservers(new Object[]{deviceID, param, value});
    }
}