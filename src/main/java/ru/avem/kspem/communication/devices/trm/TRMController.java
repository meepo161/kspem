package ru.avem.kspem.communication.devices.trm;


import ru.avem.kspem.communication.devices.DeviceController;
import ru.avem.kspem.communication.modbus.ModbusController;

import java.nio.ByteBuffer;
import java.util.Observer;


public class TRMController implements DeviceController {
    private static final short T_AMBIENT_REGISTER = 1;

    private static final int NUM_OF_WORDS_IN_REGISTER = 1;
    private static final short NUM_OF_REGISTERS = 1 * NUM_OF_WORDS_IN_REGISTER;

    private TRMModel model;
    private byte address;
    private ModbusController modbusController;
    private byte readAttempt = NUMBER_OF_READ_ATTEMPTS;
    private byte writeAttempt = NUMBER_OF_WRITE_ATTEMPTS;
    private boolean needToReed;

    public TRMController(int address, Observer observer, ModbusController controller, int deviceID) {
        this.address = (byte) address;
        model = new TRMModel(observer, deviceID);
        modbusController = controller;
    }

    @Override
    public void read(Object... args) {
        ByteBuffer inputBuffer = ByteBuffer.allocate(INPUT_BUFFER_SIZE);
        if (thereAreReadAttempts()) {
            readAttempt--;
            ModbusController.RequestStatus status = modbusController.readMultipleHoldingRegisters(
                    address, T_AMBIENT_REGISTER, NUM_OF_REGISTERS, inputBuffer);
            if (status.equals(ModbusController.RequestStatus.FRAME_RECEIVED)) {
                model.setReadResponding(true);
                resetReadAttempts();
                model.setTAmbient(inputBuffer.getShort() / 10f);
            } else {
                read(args);
            }
        }
        else {
            model.setReadResponding(false);
        }
    }

    @Override
    public void write(Object... args) {
        short register = (short) args[0];
        int numOfRegisters = (int) args[1];
        ByteBuffer inputBuffer = ByteBuffer.allocate(INPUT_BUFFER_SIZE);
        ByteBuffer dataBuffer = ByteBuffer.allocate(2 * numOfRegisters);
        for (int i = 2; i < numOfRegisters + 2; i++) {
            dataBuffer.putShort((short) ((int) args[i]));
        }
        dataBuffer.flip();

        if (thereAreWriteAttempts()) {
            writeAttempt--;
            ModbusController.RequestStatus status = modbusController.writeMultipleHoldingRegisters(
                    address, register, (short) numOfRegisters, dataBuffer, inputBuffer);
            if (status.equals(ModbusController.RequestStatus.PORT_NOT_INITIALIZED)) {
                return;
            }
            if (status.equals(ModbusController.RequestStatus.FRAME_RECEIVED)) {
                model.setWriteResponding(true);
                resetWriteAttempts();
            } else {
                write(args);
            }
        }
        else {
            model.setWriteResponding(false);
        }
    }

    @Override
    public void resetReadAttempts() {
        readAttempt = NUMBER_OF_READ_ATTEMPTS;
    }

    @Override
    public boolean thereAreReadAttempts() {
        return readAttempt > 0;
    }

    @Override
    public void resetWriteAttempts() {
        writeAttempt = NUMBER_OF_WRITE_ATTEMPTS;
    }

    @Override
    public boolean thereAreWriteAttempts() {
        return writeAttempt > 0;
    }

    @Override
    public boolean needToRead() {
        return needToReed;
    }

    @Override
    public void setNeedToRead(boolean needToRead) {
        if (needToRead) {
            model.setWriteResponding(true);
        }
        needToReed = needToRead;
    }

    public float getTemperature() {
        ByteBuffer inputBuffer = ByteBuffer.allocate(INPUT_BUFFER_SIZE);
        ModbusController.RequestStatus status = modbusController.readMultipleHoldingRegisters(
                address, T_AMBIENT_REGISTER, NUM_OF_REGISTERS, inputBuffer);
        if (status.equals(ModbusController.RequestStatus.FRAME_RECEIVED)) {
            return inputBuffer.getShort() / 10f;
        }

        return -111f;
    }
}