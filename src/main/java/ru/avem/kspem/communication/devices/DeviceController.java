package ru.avem.kspem.communication.devices;

public interface DeviceController {
    int INPUT_BUFFER_SIZE = 256;
    int NUMBER_OF_READ_ATTEMPTS = 10; 
    int NUMBER_OF_WRITE_ATTEMPTS = 10;

    int PM130_ID = 1;
    int TRM_ID = 2;
    int M40_ID = 3;
    int PR200_ID = 4;
    int DELTACP2000Object_ID = 6;
    int DELTACP2000Generator_ID = 5;

    void read(Object... args);

    void resetReadAttempts();

    boolean thereAreReadAttempts();


    void write(Object... args);

    void resetWriteAttempts();

    boolean thereAreWriteAttempts();


    boolean needToRead();

    void setNeedToRead(boolean needToRead);
}