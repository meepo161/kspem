package ru.avem.kspem.controllers

import javafx.application.Platform
import javafx.collections.FXCollections
import javafx.fxml.FXML
import javafx.scene.chart.LineChart
import javafx.scene.chart.XYChart
import javafx.scene.control.Button
import javafx.scene.control.TableColumn
import javafx.scene.control.TableView
import javafx.scene.control.TextArea
import javafx.scene.layout.AnchorPane
import javafx.scene.paint.Color
import javafx.stage.Stage
import ru.avem.kspem.Constants.Time.MILLS_IN_SEC
import ru.avem.kspem.Constants.Time.SEC_IN_MIN
import ru.avem.kspem.Main
import ru.avem.kspem.communication.CommunicationModel
import ru.avem.kspem.communication.devices.DeviceController.*
import ru.avem.kspem.communication.devices.deltaC2000.DeltaCP2000Model
import ru.avem.kspem.communication.devices.m40.M40Model
import ru.avem.kspem.communication.devices.pm130.PM130Model
import ru.avem.kspem.communication.devices.pr200.OwenPRModel
import ru.avem.kspem.controllers.MainViewController.Companion.isElevatedRotation
import ru.avem.kspem.model.Experiment1Model
import ru.avem.kspem.model.MainModel
import ru.avem.kspem.model.Point
import ru.avem.kspem.utils.Toast
import ru.avem.kspem.utils.Utils.sleep
import ru.avem.kspem.utils.View.Companion.showConfirmDialog
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class Experiment1Controller : DeviceState(), ExperimentController {

    @FXML
    lateinit var tableViewExperiment1: TableView<Experiment1Model>
    @FXML
    lateinit var tableColumnVoltageA: TableColumn<Experiment1Model, String>
    @FXML
    lateinit var tableColumnVoltageB: TableColumn<Experiment1Model, String>
    @FXML
    lateinit var tableColumnVoltageC: TableColumn<Experiment1Model, String>
    @FXML
    lateinit var tableColumnCurrentA: TableColumn<Experiment1Model, String>
    @FXML
    lateinit var tableColumnCurrentB: TableColumn<Experiment1Model, String>
    @FXML
    lateinit var tableColumnCurrentC: TableColumn<Experiment1Model, String>
    @FXML
    lateinit var tableColumnTorque: TableColumn<Experiment1Model, String>
    @FXML
    lateinit var tableColumnRotationFrequency: TableColumn<Experiment1Model, String>
    @FXML
    lateinit var tableColumnFrequency: TableColumn<Experiment1Model, String>
    @FXML
    lateinit var tableColumnPower: TableColumn<Experiment1Model, String>
    @FXML
    lateinit var tableColumnPowerActive: TableColumn<Experiment1Model, String>
    @FXML
    lateinit var tableColumnEfficiency: TableColumn<Experiment1Model, String>
    @FXML
    lateinit var tableColumnTemperature: TableColumn<Experiment1Model, String>
    @FXML
    lateinit var tableColumnCos: TableColumn<Experiment1Model, String>
    @FXML
    lateinit var tableColumnS: TableColumn<Experiment1Model, String>
    @FXML
    lateinit var tableColumnResultExperiment1: TableColumn<Experiment1Model, String>
    @FXML
    lateinit var textAreaExperiment1Log: TextArea
    @FXML
    lateinit var lineChartExperiment1: LineChart<Number, Number>
    @FXML
    lateinit var buttonStartStop: Button
    @FXML
    lateinit var buttonNext: Button
    @FXML
    lateinit var buttonCancelAll: Button
    @FXML
    lateinit var root: AnchorPane

    private val mainModel = MainModel.instance
    private val currentProtocol = mainModel.currentProtocol
    private val communicationModel = CommunicationModel.getInstance()
    private var experiment1Model: Experiment1Model? = null
    private val experiment1Data = FXCollections.observableArrayList<Experiment1Model>()

    private var dialogStage: Stage? = null
    private var isCanceled: Boolean = false

    @Volatile
    private var isNeedToRefresh: Boolean = false
    @Volatile
    private var isExperimentStart: Boolean = false
    @Volatile
    private var isExperimentEnd = true
    @Volatile
    private var isDeviceOn = true

    @Volatile
    private var isOwenPRResponding: Boolean = false
    @Volatile
    private var isDeltaObjectResponding: Boolean = false
    @Volatile
    private var isDeltaGeneratorResponding: Boolean = false
    @Volatile
    private var isDeltaReady200: Boolean = false
    @Volatile
    private var isDeltaReady0: Boolean = false
    @Volatile
    private var isPM130Responding: Boolean = false

    private val sdf = SimpleDateFormat("HH:mm:ss-SSS")
    private var logBuffer: String? = null
    @Volatile
    private var cause: String? = null
    @Volatile
    private var measuringUA: Double = 0.0
    @Volatile
    private var measuringUB: Double = 0.0
    @Volatile
    private var measuringUC: Double = 0.0
    @Volatile
    private var measuringIA: Double = 0.0
    @Volatile
    private var measuringIB: Double = 0.0
    @Volatile
    private var measuringIC: Double = 0.0
    @Volatile
    private var is1DoorOn: Boolean = false
    @Volatile
    private var is2OIOn: Boolean = false
    @Volatile
    private var is5K1_2_On: Boolean = false
    @Volatile
    private var is6KM1_2_On: Boolean = false
    @Volatile
    private var is7KM2_2_On: Boolean = false
    @Volatile
    private var isSchemeReady: Boolean = false
    @Volatile
    private var measuringTemperature: Double = 0.0
    @Volatile
    private var isTrmResponding: Boolean = false
    @Volatile
    private var isM40Responding: Boolean = false
    @Volatile
    private var measuringTorque: Double = 0.0
    @Volatile
    private var measuringTime: Double = 0.0
    @Volatile
    private var measuringCos: Double = 0.0
    @Volatile
    private var measuringSlip: Double = 0.0
    @Volatile
    private var measuringF: Double = 0.0
    @Volatile
    private var measuringS: Double = 0.0
    @Volatile
    private var measuringP: Double = 0.0
    @Volatile
    private var measuringEfficiency: Double = 0.0
    @Volatile
    private var measuringRotation: Double = 0.0
    private var isNeedToStartFP: Boolean = false

    private var torqueList: ArrayList<Double> = ArrayList()
    private var timeList: ArrayList<Double> = ArrayList()
    private var dotsList: ArrayList<Double> = ArrayList()
    private lateinit var dotsFlagsList: ArrayList<Boolean>
    private val seriesTimesAndTorques = XYChart.Series<Number, Number>()

    private var currentDot = XYChart.Series<Number, Number>()

    private var rotation = currentProtocol.rotation!!
    private var direction = currentProtocol.direction!!
    private var ratio = rotation / measuringRotation
    private var fObject = 400 * 100
    private var fGenerator = 0

    private val isThereAreAccidents: Boolean
        get() {
            if (is1DoorOn || is2OIOn || is6KM1_2_On || is7KM2_2_On || isCanceled) {
                isExperimentStart = false
                isExperimentEnd = true
            }
            return !is1DoorOn || !is2OIOn || !is6KM1_2_On || !is7KM2_2_On || !isCanceled
        }

    private val isDevicesResponding: Boolean
        get() = isOwenPRResponding && isDeltaGeneratorResponding && isDeltaObjectResponding && isPM130Responding
                && isM40Responding

    private val points = ArrayList<Point>()

    @FXML
    private fun initialize() {
        if (Main.css == "white") {
            root.stylesheets[0] = Main::class.java.getResource("styles/main_css.css").toURI().toString()
        } else {
            root.stylesheets[0] = Main::class.java.getResource("styles/main_css_black.css").toURI().toString()
        }

        experiment1Model = mainModel.experiment1Model
        experiment1Data.add(experiment1Model)
        tableViewExperiment1.items = experiment1Data
        tableViewExperiment1.selectionModel = null
        communicationModel.addObserver(this)
        torqueList = currentProtocol.torques
        timeList = currentProtocol.times
        dotsList = currentProtocol.dots
        dotsFlagsList = ArrayList(dotsList.size)
        for (i in dotsList.indices) {
            dotsFlagsList.add(true)
        }

        tableColumnVoltageA.setCellValueFactory { cellData -> cellData.value.voltageAProperty() }
        tableColumnVoltageB.setCellValueFactory { cellData -> cellData.value.voltageBProperty() }
        tableColumnVoltageC.setCellValueFactory { cellData -> cellData.value.voltageCProperty() }
        tableColumnCurrentA.setCellValueFactory { cellData -> cellData.value.currentAProperty() }
        tableColumnCurrentB.setCellValueFactory { cellData -> cellData.value.currentBProperty() }
        tableColumnCurrentC.setCellValueFactory { cellData -> cellData.value.currentCProperty() }
        tableColumnTorque.setCellValueFactory { cellData -> cellData.value.torqueProperty() }
        tableColumnRotationFrequency.setCellValueFactory { cellData -> cellData.value.rotationProperty() }
        tableColumnFrequency.setCellValueFactory { cellData -> cellData.value.frequencyProperty() }
        tableColumnPower.setCellValueFactory { cellData -> cellData.value.powerProperty() }
        tableColumnPowerActive.setCellValueFactory { cellData -> cellData.value.powerActiveProperty() }
        tableColumnEfficiency.setCellValueFactory { cellData -> cellData.value.efficiencyProperty() }
        tableColumnTemperature.setCellValueFactory { cellData -> cellData.value.temperatureProperty() }
        tableColumnCos.setCellValueFactory { cellData -> cellData.value.cosProperty() }
        tableColumnS.setCellValueFactory { cellData -> cellData.value.slipProperty() }
        tableColumnResultExperiment1.setCellValueFactory { cellData -> cellData.value.resultProperty() }

        val createLoadDiagram = createLoadDiagram()
        val createControlDots = createControlDots(createLoadDiagram)

        lineChartExperiment1.data.addAll(createLoadDiagram)
        lineChartExperiment1.data.addAll(createControlDots)
    }

    private fun createLoadDiagram(): XYChart.Series<Number, Number> {
        val seriesTimesAndTorques = XYChart.Series<Number, Number>()

        var desperateDot = 0.0

        seriesTimesAndTorques.data.add(XYChart.Data(desperateDot, currentProtocol.torques[0]))

        for (i in 0 until currentProtocol.times.size) {
            seriesTimesAndTorques.data.add(XYChart.Data(desperateDot + currentProtocol.times[i], currentProtocol.torques[i]))
            if (i != currentProtocol.times.size - 1) {
                seriesTimesAndTorques.data.add(XYChart.Data(desperateDot + currentProtocol.times[i], currentProtocol.torques[i + 1]))
            }
            desperateDot += currentProtocol.times[i]
        }

        seriesTimesAndTorques.data.add(XYChart.Data(desperateDot, 0))

        return seriesTimesAndTorques
    }

    private fun createControlDots(seriesForMerge: XYChart.Series<Number, Number>): XYChart.Series<Number, Number> {
        val controlDots = XYChart.Series<Number, Number>()

        for (dot in currentProtocol.dots) {
            var indexOfRealYValue = 0
            while (indexOfRealYValue < seriesForMerge.data.size && dot > seriesForMerge.data[indexOfRealYValue].xValue.toDouble()) {
                indexOfRealYValue++
            }
            if (indexOfRealYValue == seriesForMerge.data.size) {
                indexOfRealYValue--
                Toast.makeText("Точка $dot вышла за пределы").show(Toast.ToastType.ERROR)
            }
            controlDots.data.add(XYChart.Data(dot, seriesForMerge.data[indexOfRealYValue].yValue.toDouble()))
        }
        return controlDots
    }

    override fun setDialogStage(dialogStage: Stage) {
        this.dialogStage = dialogStage
    }

    override fun isCanceled(): Boolean {
        return isCanceled
    }

    private fun fillProtocolExperimentFields() {
        val currentProtocol = mainModel.currentProtocol
        currentProtocol.e1VoltageA = experiment1Model!!.voltageA
        currentProtocol.e1VoltageB = experiment1Model!!.voltageB
        currentProtocol.e1VoltageC = experiment1Model!!.voltageC
        currentProtocol.e1CurrentA = experiment1Model!!.currentA
        currentProtocol.e1CurrentB = experiment1Model!!.currentB
        currentProtocol.e1CurrentC = experiment1Model!!.currentC
        currentProtocol.e1Torque = experiment1Model!!.torque
        currentProtocol.e1Rotation = experiment1Model!!.rotation
        currentProtocol.e1Frequency = experiment1Model!!.frequency
        currentProtocol.e1Power = experiment1Model!!.power
        currentProtocol.e1PowerActive = experiment1Model!!.powerActive
        currentProtocol.e1Effiency = experiment1Model!!.efficiency
        currentProtocol.e1Temperature = experiment1Model!!.temperature
        currentProtocol.e1Effiency = experiment1Model!!.cos
        currentProtocol.e1Temperature = experiment1Model!!.slip
    }

    @FXML
    private fun handleNextExperiment() {
        fillProtocolExperimentFields()
        dialogStage!!.close()
    }

    @FXML
    private fun handleStartExperiment() {
        if (isExperimentEnd) {
            startExperiment()
        } else {
            stopExperiment()
        }
    }

    @FXML
    private fun handleExperimentCancel() {
        isExperimentStart = false
        dialogStage!!.close()
    }

    private fun stopExperiment() {
        isNeedToRefresh = false
        buttonStartStop.isDisable = true
        cause = "Отменено оператором"
        isExperimentStart = false
    }


    private fun startExperiment() {
        points.clear()
        isNeedToRefresh = true
        isExperimentStart = true
        isExperimentEnd = false
        buttonStartStop.text = "Остановить"
        buttonNext.isDisable = true
        buttonCancelAll.isDisable = true
        experiment1Model!!.clearProperties()
        isDeviceOn = false
        isSchemeReady = false
        is1DoorOn = false
        is2OIOn = false
        is5K1_2_On = false
        is6KM1_2_On = false
        is7KM2_2_On = false
        cause = ""

        Thread {
            Platform.runLater {
                appendOneMessageToLog("Включите кнопочный пост и проверьте правильность направления вращений")
            }
            communicationModel.finalizeDevicesWithoutPR()
            communicationModel.initExperiment1Devices()
            communicationModel.onKM6ButtonPost()
            communicationModel.resetK1_2()

            while (isExperimentStart && !is5K1_2_On) {
                sleep(10)
            }


            if (isExperimentStart && !checkRotations()) {
                if (isExperimentStart && !checkRotations()) {
                    cause = "Проверьте правильность выбора направления вращения двигателя или соединения"
                    isExperimentStart = false
                }
            }

            if (isExperimentStart) {
                communicationModel.offKM6ButtonPost()
                communicationModel.resetK1_2()
                appendOneMessageToLog("Начало испытания")
                measuringUA = 0.0
                measuringIA = 0.0
                measuringF = 0.0
                measuringRotation = 0.0
                measuringTemperature = 0.0
                measuringS = 0.0
                communicationModel.resetK1_2()
            }

            isDeviceOn = true

            while (isExperimentStart && !isDevicesResponding) {
                appendOneMessageToLog(getNotRespondingDevicesString("Нет связи с устройствами "))
                sleep(100)
            }

            if (isExperimentStart && isOwenPRResponding) {
                appendOneMessageToLog("Инициализация кнопочного поста...")
                communicationModel.onKM6ButtonPost()
            }

            while (isExperimentStart && !is5K1_2_On) {
                appendOneMessageToLog("Включите кнопочный пост")
            }

            if (isExperimentStart && !isThereAreAccidents) {
                appendOneMessageToLog(getAccidentsString("Аварии"))
                isExperimentStart = false
            }

            isSchemeReady = true

            if (isExperimentStart && isDevicesResponding) {
                appendOneMessageToLog("Инициализация испытания")
            }

//            if (isExperimentStart && isDevicesResponding) {
//                appendOneMessageToLog("Устанавливаем начальные точки для ЧП объекта")
//                communicationModel.setObjectParams(40000, 210 * 10, 40000, 2 * 10, 200)
//                sleep(200)
//                appendOneMessageToLog("Устанавливаем начальные точки для ЧП генератора")
//                communicationModel.setGeneratorParams(40000, 220 * 10, 40000, 2 * 10, 200)
//                sleep(200)
//            }

            if (isExperimentStart && isDevicesResponding) {
                appendOneMessageToLog("Запускаем ЧП объекта")
                communicationModel.setObjectFCur(fObject)
                sleep(200)
                appendOneMessageToLog("Ожидаем, пока частотный преобразователь выйдет к заданным характеристикам")
                communicationModel.startObject()
            }

            var time = 1000.0
            while (isExperimentStart && time > 0 && isDevicesResponding) {
                time -= 100.0
                sleep((100 - 1).toLong())
            }

            measuringRotation = 0.0
            if (isExperimentStart) {
                ratio = rotation / measuringRotation
            }

            while (isExperimentStart && ratio > 1.05 && isDevicesResponding) {
                ratio = rotation / measuringRotation
                sleep((100 - 1).toLong())
            }

            time = 2000.0
            while (isExperimentStart && time > 0 && isDevicesResponding) {
                time -= 100.0
                sleep((100 - 1).toLong())
            }

            if (isExperimentStart && isDevicesResponding) {
                fGenerator = (measuringRotation / 30).toInt() * 100
                appendOneMessageToLog("Двигатель набрал заданную частоту вращения")
                sleep(100)
                communicationModel.setGeneratorFCur(fGenerator)
                sleep(100)
                appendOneMessageToLog("Запускам ЧП генератора")
                if (ratio > 0.8 && ratio < 1.2) {
                    if (direction == "left") {
                        sleep(100)
                        communicationModel.startGenerator()
                    } else if (direction == "right") {
                        sleep(100)
                        communicationModel.startGenerator()
                    } else {
                        isExperimentStart = false
                        cause = "Неверное направление вращения объекта"
                    }
                } else {
                    cause = "Частота вращения не соответсвует"
                    isExperimentStart = false
                }
            }

            var timeSum = 0.0

            var totalTime = 0.0

            for (i in torqueList.indices) {
                var timePassed = 0.0
                if (isExperimentStart && isDevicesResponding) {
                    appendOneMessageToLog("Началась регулировка")
                    if (torqueList[i] == 0.0) {
                        appendOneMessageToLog("Остановка частотных преобразователей")
                        appendOneMessageToLog("Перерыв ${timeList[i]} минут")
                        communicationModel.stopObject()
                        sleep(100)
                        communicationModel.stopGenerator()
                        isNeedToStartFP = true
                    } else {
                        if (isNeedToStartFP && isDevicesResponding && isExperimentStart) {
                            communicationModel.startObject()
                            sleep(500)
                            appendOneMessageToLog(measuringTorque.toString())
                            do {
                                ratio = rotation / measuringRotation
                                sleep((100 - 1).toLong())
                            } while (isExperimentStart && ratio > 1.05 && isDevicesResponding)
                            communicationModel.startGenerator()
                            isNeedToStartFP = false
                        }
                        regulationTorque(torqueList[i])
                        appendOneMessageToLog("Регулировка окончена")
                    }
                }

                time = timeList[i] * SEC_IN_MIN * MILLS_IN_SEC

                while (isExperimentStart && timePassed < time && isDevicesResponding) {
                    sleep((100 - 1).toLong())
                    timePassed += 100.0
                    totalTime += 100.0

                    drawDot(timeSum, timeSum + currentProtocol.times[i], currentProtocol.torques[i], timePassed / time)
                    measuringTime = currentProtocol.times[i]
                    for (j in dotsList.indices) {
                        if (dotsFlagsList[j] && (dotsList[j] * SEC_IN_MIN * MILLS_IN_SEC < totalTime)) {
                            Platform.runLater {
                                fillPointData()
                                dotsFlagsList[j] = false
                            }
                        }
                    }


                }
                timeSum += currentProtocol.times[i]
            } //циклограмма, если момент 0.0 - перерыв

            if (MainViewController.isElevatedRotation && isExperimentStart) {

                appendOneMessageToLog("Запуск опыта повышенной частоты")
                var freqObject = 40500

                do {
                    freqObject += 500
                    val freqGenerator = freqObject * 2 / 3
                    communicationModel.setObjectFCur(freqObject)
                    sleep(20)
                    communicationModel.setGeneratorFCur(freqGenerator)
                    sleep(300)
                } while (freqObject < 50000)


                time = 4000.0
                while (isExperimentStart && time > 0 && isDevicesResponding) {
                    time -= 100.0
                    sleep((100 - 1).toLong())
                }

                if (isExperimentStart && isDevicesResponding) {
                    appendOneMessageToLog("Началась регулировка для проведения опыта повышенной частоты")
                    regulationTorque(0.0)
                    appendOneMessageToLog("Регулировка окончена")
                }


                time = 120000.0
                while (isExperimentStart && time > 0 && isDevicesResponding) {
                    time -= 100.0
                    sleep((100 - 1).toLong())
                }

                if (isExperimentStart && isDevicesResponding) {
                    appendOneMessageToLog("Проверка номинальных параметров при 260 В, 500 Гц")
                    fillPointData()
                }


            } //260 В 500 Гц 2минуты, снять параметры, если стоит галочка

            if (MainViewController.isXXSelected && isExperimentStart) {

                appendOneMessageToLog("Запуск опыта холостого хода")
                var freqObject = if (isElevatedRotation) {
                    50000
                } else {
                    40000
                }

                while (freqObject >= 40000) {
                    freqObject -= 500
                    val freqGenerator = freqObject * 2 / 3
                    communicationModel.setObjectFCur(freqObject)
                    sleep(20)
                    communicationModel.setGeneratorFCur(freqGenerator)
                    sleep(300)
                }

                if (isExperimentStart && isDevicesResponding) {
                    appendOneMessageToLog("Началась регулировка для проведения опыта холостого хода")
                    regulationTorque(0.0)
                    appendOneMessageToLog("Регулировка окончена")
                }

                time = 4000.0
                while (isExperimentStart && time > 0 && isDevicesResponding) {
                    time -= 100.0
                    sleep((100 - 1).toLong())
                }

                if (isExperimentStart && isDevicesResponding) {
                    appendOneMessageToLog("Проверка параметров холостого хода  при 200 В, 400 Гц")
                    fillPointData()
                }
            } //200 В 400 Гц, снять параметры, закончить, если выбрана галочка XX

            isNeedToRefresh = false

            if (isExperimentStart) experiment1Model!!.temperature = String.format("%.2f", communicationModel.trmTemperature)

            isExperimentStart = false
            isExperimentEnd = true
            sleep(500)
            communicationModel.stopObject()
            communicationModel.stopGenerator()
            while (isExperimentStart && !isDeltaReady0) {
                sleep(100)
                appendOneMessageToLog("Ожидаем, пока частотный преобразователь остановится")
            }


            communicationModel.resetK1_2()
            sleep(50)
            isNeedToRefresh = false
            communicationModel.offAllKms() //разбираем все возможные схемы
            sleep(50)
            communicationModel.finalizeAllDevices() //прекращаем опрашивать устройства

            if (cause != "") {
                appendMessageToLog(String.format("Испытание прервано по причине: %s", cause))
                experiment1Model!!.result = "Неуспешно"
            } else if (!isDevicesResponding) {
                appendMessageToLog(getNotRespondingDevicesString("Испытание прервано по причине: потеряна связь с устройствами"))
                experiment1Model!!.result = "Неуспешно"
            } else {
                experiment1Model!!.result = "Успешно"
                appendMessageToLog("Испытание завершено успешно")
            }
            appendMessageToLog("\n------------------------------------------------\n")

            currentProtocol.points = points

            Platform.runLater()
            {
                buttonStartStop.text = "Запустить"
                buttonStartStop.isDisable = false
                buttonNext.isDisable = false
                buttonCancelAll.isDisable = false
            }

        }.start()
    }

    private fun checkRotations(): Boolean {
        var isRotationChecked = false
        var isVseOk = false
        communicationModel.setObjectParams(40000, 295 * 10, 40000, 2 * 10, 200)
        sleep(300)
        communicationModel.setGeneratorParams(40000, 260 * 10, 40000, 2 * 10, 200)
        sleep(300)
        communicationModel.setObjectFCur(3500)
        communicationModel.setGeneratorFCur(2500)
        communicationModel.startObject()
        sleep(1000)
        communicationModel.stopObject()
        sleep(5000)
        communicationModel.startGenerator()
        sleep(1000)
        communicationModel.stopGenerator()
        sleep(5000)

        Platform.runLater {
            showConfirmDialog("Направление вращений двигателя и генератора совпадает?",
                    {
                        isRotationChecked = true
                        isVseOk = true
                    },
                    {
                        communicationModel.changeRotationGenerator()
                        isRotationChecked = true
                        isVseOk = false
                    }
            )
        }

        while (!isRotationChecked) {
            sleep(100)
        }

        return isVseOk

    }

    private fun fillPointData() {
        points.add(Point(
                measuringUA,
                measuringUB,
                measuringUC,
                measuringIA,
                measuringIB,
                measuringIC,
                measuringTorque,
                measuringRotation,
                measuringF,
                measuringS,
                measuringP,
                measuringP / measuringS * 100,
                communicationModel.trmTemperature,
                measuringTime,
                measuringCos,
                (currentProtocol.rotation - measuringRotation) / currentProtocol.rotation))

        Platform.runLater {
            experiment1Data.add(Experiment1Model(
                    String.format("%.2f", measuringUA),
                    String.format("%.2f", measuringUB),
                    String.format("%.2f", measuringUC),
                    String.format("%.2f", measuringIA),
                    String.format("%.2f", measuringIB),
                    String.format("%.2f", measuringIC),
                    String.format("%.2f", measuringTorque),   //this.torque
                    String.format("%.2f", measuringRotation),   //this.rotation
                    String.format("%.2f", measuringF),   //this.frequency
                    String.format("%.2f", measuringS),   //this.power
                    String.format("%.2f", measuringP),   //this.powerActive
                    String.format("%.2f", measuringP / measuringS * 100), //this.efficiency
                    String.format("%.2f", communicationModel.trmTemperature),   //this.temperature
                    String.format("%.2f", measuringTime),   //this.timeExpr
                    String.format("%.2f", measuringCos),   //this.cos
                    String.format("%.2f", (currentProtocol.rotation - measuringRotation) / currentProtocol.rotation),   //this.slip
                    ""))
        }

    }

    private fun drawDot(x1: Double, x2: Double, y: Double, percent: Double) {
        Platform.runLater {
            lineChartExperiment1.data.removeAll(currentDot)
            currentDot = createCurrentDot(x1, x2, y, percent)
            lineChartExperiment1.data.addAll(currentDot)
        }
    }

    private fun createCurrentDot(x1: Double, x2: Double, y: Double, percent: Double): XYChart.Series<Number, Number> {
        val currentDot = XYChart.Series<Number, Number>()
        currentDot.data.add(XYChart.Data(x1 + (x2 - x1) * percent, y))
        return currentDot
    }

    private fun appendMessageToLog(message: String) {
        Platform.runLater {
            textAreaExperiment1Log.appendText(String.format("%s \t| %s\n", sdf.format(System.currentTimeMillis()), message))
        }
    }

    private fun appendOneMessageToLog(message: String) {
        if (logBuffer == null || logBuffer != message) {
            logBuffer = message
            appendMessageToLog(message)
        }
    }

    private fun regulationTorque(torque: Double) {
        val coarseMinLimit = torque - 0.3
        val coarseMaxLimit = torque + 0.3
        val fineMinLimit = torque - 0.05
        val fineMaxLimit = torque + 0.05
        while (isExperimentStart && (measuringTorque < coarseMinLimit || measuringTorque > coarseMaxLimit) && isDevicesResponding) {

            appendOneMessageToLog("Регулируем частоту для получения заданного момента грубо")
            if (measuringTorque < coarseMinLimit) {
                fGenerator -= 100
                communicationModel.setGeneratorFCur(fGenerator)
            } else if (measuringTorque > coarseMaxLimit) {
                fGenerator += 100
                communicationModel.setGeneratorFCur(fGenerator)
            }
            var time = 1500.0
            while (isExperimentStart && time > 0 && isDevicesResponding) {
                time -= 100.0
                sleep((100 - 1).toLong())
            }
        }
        while (isExperimentStart && (measuringTorque < fineMinLimit || measuringTorque > fineMaxLimit) && isDevicesResponding) {
            appendOneMessageToLog("Регулируем частоту для получения заданного момента точно")
            if (measuringTorque < fineMinLimit) {
                fGenerator -= 20
                communicationModel.setGeneratorFCur(fGenerator)
            } else if (measuringTorque > fineMaxLimit) {
                fGenerator += 20
                communicationModel.setGeneratorFCur(fGenerator)
            }
            var time = 1700.0
            while (isExperimentStart && time > 0 && isDevicesResponding) {
                time -= 100.0
                sleep((100 - 1).toLong())
            }
        }

    }


    private fun getAccidentsString(mainText: String): String {
        return String.format("%s: %s%s%s%s%s",
                mainText,
                if (is1DoorOn) "" else "открыта дверь, ",
                if (is2OIOn) "" else "открыты концевики ОИ, ",
                if (is6KM1_2_On) "" else "не замкнулся КМ1, ",
                if (is7KM2_2_On) "" else "не замкнулся КМ2, ",
                if (isCanceled) "" else "нажата кнопка отмены, ")
    }

    private fun getNotRespondingDevicesString(mainText: String): String {
        return String.format("%s %s%s%s%s%s",
                mainText,
                if (isOwenPRResponding) "" else "Овен ПР ",
                if (isM40Responding) "" else "М40 ",
                if (isDeltaObjectResponding) "" else "Дельта объекта ",
                if (isDeltaGeneratorResponding) "" else "Дельта генератора ",
                if (isPM130Responding) "" else "ПМ130 ")
    }


    override fun update(o: Observable, values: Any) {
        val modelId = (values as Array<Any>)[0] as Int
        val param = values[1] as Int
        val value = values[2]

        when (modelId) {
            PR200_ID -> when (param) {
                OwenPRModel.RESPONDING_PARAM -> {
                    isOwenPRResponding = value as Boolean
                    Platform.runLater { deviceStateCirclePR200.fill = if (value) Color.LIME else Color.RED }
                }

                OwenPRModel.DI1_DOORS -> {
                    is1DoorOn = value as Boolean
                    if (is1DoorOn) {
                        cause = "DI1_DOORS"
                        isExperimentStart = false
                        communicationModel.stopObject()
                        communicationModel.stopGenerator()
                    }
                }
                OwenPRModel.DI2_OI -> {
                    is2OIOn = value as Boolean
                    if (is2OIOn) {
                        cause = "DI2_OI"
                        isExperimentStart = false
                        communicationModel.stopObject()
                        communicationModel.stopGenerator()
                    }
                }
                OwenPRModel.DI5_K1_2 -> {
                    is5K1_2_On = value as Boolean
                    if (isSchemeReady && !is5K1_2_On) {
                        cause = "нажата кнопка отмены"
                        isExperimentStart = false
                    }
                }
                OwenPRModel.DI6_KM1_2 -> {
                    is6KM1_2_On = value as Boolean
                    if (is6KM1_2_On) {
                        cause = "DI6_KM1_2"
                        isExperimentStart = false
                        communicationModel.stopObject()
                        communicationModel.stopGenerator()
                    }

                }
                OwenPRModel.DI7_KM2_2 -> {
                    is7KM2_2_On = value as Boolean
                    if (is7KM2_2_On) {
                        cause = "DI7_KM2_2"
                        isExperimentStart = false
                        communicationModel.stopObject()
                        communicationModel.stopGenerator()
                    }
                }
            }

            M40_ID -> when (param) {
                M40Model.RESPONDING_PARAM -> {
                    isM40Responding = value as Boolean
                    Platform.runLater { deviceStateCircleM40.fill = if (value) Color.LIME else Color.RED }
                }
                M40Model.TORQUE_PARAM -> if (isNeedToRefresh) {
                    measuringTorque = value as Float - 0.14
                    val torque = String.format("%.2f", measuringTorque)
                    experiment1Model!!.torque = torque
                }
                M40Model.ROTATION_FREQUENCY_PARAM -> if (isNeedToRefresh) {
                    measuringRotation = (value as Float).toDouble()
                    val rotation = String.format("%.2f", measuringRotation)
                    experiment1Model!!.rotation = rotation
                }
            }

            PM130_ID -> when (param) {
                PM130Model.RESPONDING_PARAM -> {
                    isPM130Responding = value as Boolean
                    Platform.runLater { deviceStateCirclePM130.fill = if (value) Color.LIME else Color.RED }
                }
                PM130Model.V1_PARAM -> if (isNeedToRefresh) {
                    measuringUA = (value as Float).toDouble()
                    val UOutA = String.format("%.2f", measuringUA)
                    experiment1Model!!.voltageA = UOutA
                }
                PM130Model.V2_PARAM -> if (isNeedToRefresh) {
                    measuringUB = (value as Float).toDouble()
                    val UOutB = String.format("%.2f", measuringUB)
                    experiment1Model!!.voltageB = UOutB
                }
                PM130Model.V3_PARAM -> if (isNeedToRefresh) {
                    measuringUC = (value as Float).toDouble()
                    val UOutC = String.format("%.2f", measuringUC)
                    experiment1Model!!.voltageC = UOutC
                }
                PM130Model.I1_PARAM -> if (isNeedToRefresh) {
                    measuringIA = (value as Float).toDouble()
                    val IOutA = String.format("%.2f", measuringIA)
                    if (measuringIA > 0.5) {
                        experiment1Model!!.currentA = IOutA
                    }
                }
                PM130Model.I2_PARAM -> if (isNeedToRefresh) {
                    measuringIB = (value as Float).toDouble()
                    val IOutB = String.format("%.2f", measuringIB)
                    if (measuringIB > 0.5) {
                        experiment1Model!!.currentB = IOutB
                    }
                }
                PM130Model.I3_PARAM -> if (isNeedToRefresh) {
                    measuringIC = (value as Float).toDouble()
                    val IOutC = String.format("%.2f", measuringIC)
                    if (measuringIC > 0.5) {
                        experiment1Model!!.currentC = IOutC
                    }
                }
                PM130Model.F_PARAM -> if (isNeedToRefresh) {
                    measuringF = (value as Float).toDouble()
                    val FOut = String.format("%.2f", measuringF)
                    if (measuringF > 300) {
                        experiment1Model!!.frequency = FOut
                    }
                }
                PM130Model.P_PARAM -> if (isNeedToRefresh) {
                    measuringP = (value as Float).toDouble()
                    val POut = String.format("%.2f", measuringP)
                    experiment1Model!!.powerActive = POut
                }
                PM130Model.COS_PARAM -> if (isNeedToRefresh) {
                    measuringCos = (value as Float).toDouble()
                    val Cos = String.format("%.2f", measuringCos)
                    experiment1Model!!.cos = Cos
                }
                PM130Model.S_PARAM -> if (isNeedToRefresh) {
                    measuringS = (value as Float).toDouble()
                    val SOut = String.format("%.2f", measuringS)
                    experiment1Model!!.power = SOut

                    val EFFOut = String.format("%.2f", measuringP / measuringS * 100)
                    experiment1Model!!.efficiency = EFFOut

                    val slip = String.format("%.2f", (currentProtocol.rotation - measuringRotation) / currentProtocol.rotation)
                    experiment1Model!!.slip = slip
                }
            }

            DELTACP2000Object_ID -> when (param) {
                DeltaCP2000Model.RESPONDING_PARAM -> {
                    isDeltaObjectResponding = value as Boolean
                    Platform.runLater { deviceStateCircleCPED.fill = if (value) Color.LIME else Color.RED }
                }
                DeltaCP2000Model.CURRENT_FREQUENCY_PARAM -> setCurrentFrequencyObject(value as Short)
            }


            DELTACP2000Generator_ID -> when (param) {
                DeltaCP2000Model.RESPONDING_PARAM -> {
                    isDeltaGeneratorResponding = value as Boolean
                    Platform.runLater { deviceStateCircleCPEG.fill = if (value) Color.LIME else Color.RED }
                }
                DeltaCP2000Model.CURRENT_FREQUENCY_PARAM -> setCurrentFrequencyGenerator(value as Short)
            }
        }
    }

    private fun setCurrentFrequencyObject(value: Short) {
        isDeltaReady200 = value.toInt() == 20000
        isDeltaReady0 = value.toInt() == 0
    }

    private fun setCurrentFrequencyGenerator(value: Short) {
        isDeltaReady200 = value.toInt() == 20000
        isDeltaReady0 = value.toInt() == 0
    }

}
