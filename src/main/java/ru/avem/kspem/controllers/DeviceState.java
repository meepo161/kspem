package ru.avem.kspem.controllers;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import ru.avem.kspem.communication.devices.deltaC2000.DeltaCP2000Model;
import ru.avem.kspem.communication.devices.m40.M40Model;
import ru.avem.kspem.communication.devices.pm130.PM130Model;
import ru.avem.kspem.communication.devices.pr200.OwenPRModel;
import ru.avem.kspem.communication.devices.trm.TRMModel;

import java.util.Observable;
import java.util.Observer;

import static ru.avem.kspem.communication.devices.DeviceController.*;

public class DeviceState implements Observer {

    @FXML
    protected Circle deviceStateCirclePM130;
    @FXML
    protected Circle deviceStateCircleM40;
    @FXML
    protected Circle deviceStateCirclePR200;
    @FXML
    protected Circle deviceStateCircleCPED;
    @FXML
    protected Circle deviceStateCircleCPEG;
    @FXML
    protected Circle deviceStateCircleTrm;


    @Override
    public void update(Observable o, Object values) {
        int modelId = (int) (((Object[]) values)[0]);
        int param = (int) (((Object[]) values)[1]);
        Object value = (((Object[]) values)[2]);

        switch (modelId) {
            case PM130_ID:
                if (param == PM130Model.RESPONDING_PARAM) {
                    Platform.runLater(() -> deviceStateCirclePM130.setFill(((boolean) value) ? Color.LIME : Color.RED));
                }
                break;
            case PR200_ID:
                if (param == OwenPRModel.RESPONDING_PARAM) {
                    Platform.runLater(() -> deviceStateCirclePR200.setFill(((boolean) value) ? Color.LIME : Color.RED));
                }
                break;
            case M40_ID:
                if (param == M40Model.RESPONDING_PARAM) {
                    Platform.runLater(() -> deviceStateCircleM40.setFill(((boolean) value) ? Color.LIME : Color.RED));
                }
                break;
            case DELTACP2000Object_ID:
                if (param == DeltaCP2000Model.RESPONDING_PARAM) {
                    Platform.runLater(() -> deviceStateCircleCPED.setFill(((boolean) value) ? Color.LIME : Color.RED));
                }
                break;
            case DELTACP2000Generator_ID:
                if (param == DeltaCP2000Model.RESPONDING_PARAM) {
                    Platform.runLater(() -> deviceStateCircleCPEG.setFill(((boolean) value) ? Color.LIME : Color.RED));
                }
                break;
            case TRM_ID:
                if (param == TRMModel.RESPONDING_PARAM) {
                    Platform.runLater(() -> deviceStateCircleTrm.setFill(((boolean) value) ? Color.LIME : Color.RED));
                }
                break;
        }
    }
}
