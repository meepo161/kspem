package ru.avem.kspem.controllers

import javafx.fxml.FXML
import javafx.scene.layout.AnchorPane
import ru.avem.kspem.Main
import ru.avem.kspem.Main.Companion.css
import ru.avem.kspem.communication.CommunicationModel
import ru.avem.kspem.utils.Utils

class DeviceStateWindowController : DeviceState() {

    @FXML
    lateinit var root: AnchorPane

    var flag : Boolean = true

    @FXML
    private fun initialize() {
        if (css == "white") {
            root.getStylesheets().set(0, Main::class.java.getResource("styles/main_css.css").toURI().toString())
        } else {
            root.getStylesheets().set(0, Main::class.java.getResource("styles/main_css_black.css").toURI().toString())
        }
        val communicationModel = CommunicationModel.getInstance()
        communicationModel.addObserver(this)

        flag = true
        Thread {
            while (flag) {
                communicationModel.resetAllDevices()
                Utils.sleep(1000)
            }
            communicationModel.finalizeAllDevices()
            communicationModel.deleteObservers()
        }.start()
    }
}
