package ru.avem.kspem.states.main

interface State {
    fun toIdleState()

    fun toWaitState()

    fun toResultState()
}
