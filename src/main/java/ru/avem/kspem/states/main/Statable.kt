package ru.avem.kspem.states.main


interface Statable {
    fun toIdleState()

    fun toWaitState()

    fun toResultState()
}
