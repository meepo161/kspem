package ru.avem.kspem.model

import ru.avem.kspem.db.model.Account
import ru.avem.kspem.db.model.Protocol
import ru.avem.kspem.db.model.TestItem

class MainModel private constructor() {

    private var firstTester = Account("ADMIN", "ADMIN", "ADMIN", "ADMIN", "ADMIN")
    private var secondTester = Account("ADMIN", "ADMIN", "ADMIN", "ADMIN", "ADMIN")

    var isNeedRefresh = true
    var currentProtocol: Protocol = Protocol()

    private lateinit var intermediateProtocol: Protocol

    var experiment1Model = Experiment1Model()

    fun setTesters(tester1: Account, tester2: Account) {
        this.firstTester = tester1
        this.secondTester = tester2
    }

    fun createNewProtocol(serialNumber: String, selectedTestItem: TestItem) {
        currentProtocol = Protocol(serialNumber, selectedTestItem, firstTester, secondTester, System.currentTimeMillis())
    }

    fun setIntermediateProtocol(intermediateProtocol: Protocol) {
        this.intermediateProtocol = intermediateProtocol
    }

    fun applyIntermediateProtocol() {
        currentProtocol = intermediateProtocol
        intermediateProtocol = Protocol()
    }

    companion object {
        val instance = MainModel()
    }
}
