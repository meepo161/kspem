package ru.avem.kspem.logging

import org.apache.poi.openxml4j.exceptions.InvalidFormatException
import org.apache.poi.ss.usermodel.*
import org.apache.poi.xssf.usermodel.XSSFCellStyle
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import ru.avem.kspem.Main
import ru.avem.kspem.db.model.Protocol
import ru.avem.kspem.model.Point
import ru.avem.kspem.utils.Toast
import ru.avem.kspem.utils.Utils.copyFileFromStream
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat

object Logging {

    const val TO_DESIRED_ROW = 0

    fun getTempWorkbook(protocol: Protocol): File? {
        return writeWorkbookToTempFile(protocol)
    }

    private fun writeWorkbookToTempFile(protocol: Protocol): File {
        clearDirectory()
        val sdf = SimpleDateFormat("dd_MM(HH-mm-ss)")
        val fileName = "protocol-" + sdf.format(System.currentTimeMillis()) + ".xlsx"

        val file = File("protocol", fileName)
        if (!writeWorkbookToFile(protocol, file)) {
            Toast.makeText("Произошла ошибка при попытке отображения протокола").show(Toast.ToastType.ERROR)
        }
        return file
    }

    private fun clearDirectory() {
        val directory = File("protocol")
        if (!directory.exists()) {
            directory.mkdir()
        } else if (directory.listFiles() != null) {
            for (child in directory.listFiles()!!) {
                child.delete()
            }
        }
    }

    fun writeWorkbookToFile(protocol: Protocol, file: File): Boolean {
        try {
            val out = convertProtocolToWorkbook(protocol)

            val fileOut = FileOutputStream(file)
            out.writeTo(fileOut)
            out.close()
            fileOut.close()
        } catch (e: IOException) {
            return false
        } catch (e: InvalidFormatException) {
            return false
        }

        return true
    }

    @Throws(IOException::class, InvalidFormatException::class)
    private fun convertProtocolToWorkbook(protocol: Protocol): ByteArrayOutputStream {
        val templateTempFile = File(System.getProperty("user.dir"), "tmp.xlsx")
        try {
            copyFileFromStream(Main::class.java.getResourceAsStream("raw/template_kspem.xlsx"), templateTempFile)
        } catch (e: IOException) {
            Toast.makeText("Ошибка").show(Toast.ToastType.ERROR)
        }

        XSSFWorkbook(templateTempFile).use { wb ->
            val sheet = wb.getSheetAt(0)
            for (i in 0..99) {
                val row = sheet.getRow(i)
                if (row != null) {
                    for (j in 0..19) {
                        val cell = row.getCell(j)
                        if (cell != null && cell.cellTypeEnum == CellType.STRING) {
                            when (cell.stringCellValue) {
                                "\$PROTOCOL_NUMBER$" -> {
                                    val id = protocol.id
                                    if (id != 0L) {
                                        cell.setCellValue(id.toString() + "")
                                    } else {
                                        cell.setCellValue("")
                                    }
                                }
                                "\$OBJECT$" -> {
                                    val objectName = protocol.type
                                    if (objectName != null) {
                                        cell.setCellValue(objectName)
                                    } else {
                                        cell.setCellValue("")
                                    }
                                }
                                "\$SERIAL_NUMBER$" -> {
                                    val serialNumber = protocol.serialNumber
                                    if (serialNumber != null && !serialNumber.isEmpty()) {
                                        cell.setCellValue(serialNumber)
                                    } else {
                                        cell.setCellValue("")
                                    }
                                }
                                "$101$" -> {
                                    cell.setCellValue(protocol.type)
                                }
                                "$102$" -> {
                                    cell.setCellValue(protocol.torque)
                                }
                                "$103$" -> {
                                    cell.setCellValue(protocol.power)
                                }
                                "$104$" -> {
                                    cell.setCellValue(protocol.voltage)
                                }
                                "$105$" -> {
                                    cell.setCellValue(protocol.averageCurrent)
                                }
                                "$106$" -> {
                                    cell.setCellValue(protocol.noLoadCurrent)
                                }
                                "$107$" -> {
                                    cell.setCellValue(protocol.rotation)
                                }
                                "$108$" -> {
                                    cell.setCellValue(protocol.kpd)
                                }
                                "$109$" -> {
                                    cell.setCellValue(protocol.temperature)
                                }
                                "\$POS1$" -> cell.setCellValue(protocol.position1)
                                "\$POS2$" -> cell.setCellValue(protocol.position2)
                                "\$POS1NAME$" -> cell.setCellValue(String.format("/%s/", protocol.position1FullName))
                                "\$POS2NAME$" -> cell.setCellValue(String.format("/%s/", protocol.position2FullName))
                                "\$DATE$" -> {
                                    val sdf = SimpleDateFormat("dd-MM-yy")
                                    cell.setCellValue(sdf.format(protocol.millis))
                                }
                                else -> if (cell.stringCellValue.contains("$")) {
                                    cell.setCellValue("")
                                }

                            }

                        }
                    }
                }

            }
            fillParameters(wb, protocol.points)
            val out = ByteArrayOutputStream()
            try {
                wb.write(out)
            } finally {
                out.close()
            }
            return out
        }
    }

    private fun fillParameters(wb: XSSFWorkbook, points: ArrayList<Point>) {
        val sheet = wb.getSheetAt(0)
        var row: Row
        var cellStyle: XSSFCellStyle = generateStyles(wb) as XSSFCellStyle
        var rowNum = sheet.lastRowNum - TO_DESIRED_ROW
        row = sheet.createRow(rowNum)
        var columnNum = 0
        for (i in points.indices) {
            columnNum = fillOneCell(row, columnNum, cellStyle, points[i].measuringUA)
            columnNum = fillOneCell(row, columnNum, cellStyle, points[i].measuringUB)
            columnNum = fillOneCell(row, columnNum, cellStyle, points[i].measuringUC)
            columnNum = fillOneCell(row, columnNum, cellStyle, points[i].measuringIA)
            columnNum = fillOneCell(row, columnNum, cellStyle, points[i].measuringIB)
            columnNum = fillOneCell(row, columnNum, cellStyle, points[i].measuringIC)
            columnNum = fillOneCell(row, columnNum, cellStyle, points[i].measuringTorque)
            columnNum = fillOneCell(row, columnNum, cellStyle, points[i].measuringRotation)
            columnNum = fillOneCell(row, columnNum, cellStyle, points[i].measuringF)
            columnNum = fillOneCell(row, columnNum, cellStyle, points[i].measuringS)
            columnNum = fillOneCell(row, columnNum, cellStyle, points[i].measuringP)
            columnNum = fillOneCell(row, columnNum, cellStyle, points[i].measuringEfficiency)
            columnNum = fillOneCell(row, columnNum, cellStyle, points[i].trmTemperature.toDouble())
            columnNum = fillOneCell(row, columnNum, cellStyle, points[i].measuringTime)
            columnNum = fillOneCell(row, columnNum, cellStyle, points[i].measuringCos)
            columnNum = fillOneCell(row, columnNum, cellStyle, points[i].measuringSlip)
            row = sheet.createRow(++rowNum)
            columnNum = 0
        }

    }

    private fun fillOneCell(row: Row, columnNum: Int, cellStyle: XSSFCellStyle, points: Double): Int {
        val cell: Cell = row.createCell(columnNum)
        cell.cellStyle = cellStyle
        cell.setCellValue(String.format("%.2f", points))
        return columnNum + 1
    }

    private fun generateStyles(wb: XSSFWorkbook): CellStyle {
        val headStyle: CellStyle = wb.createCellStyle()
        headStyle.wrapText = true
        headStyle.borderBottom = BorderStyle.THIN
        headStyle.borderTop = BorderStyle.THIN
        headStyle.borderLeft = BorderStyle.THIN
        headStyle.borderRight = BorderStyle.THIN
        headStyle.alignment = HorizontalAlignment.CENTER
        headStyle.verticalAlignment = VerticalAlignment.CENTER
        return headStyle
    }

}